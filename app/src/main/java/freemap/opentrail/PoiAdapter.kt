package freemap.opentrail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val tvTitle : TextView = view.findViewById(R.id.tvListItemTitle)
    val tvDescription : TextView = view.findViewById(R.id.tvListItemDescription)
}

class PoiAdapter: RecyclerView.Adapter<ViewHolder>() {

    private var titles = mutableListOf<String>()
    private var descriptions =mutableListOf<String>()
    var onListItemClick: ((Int)->Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.listitem, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTitle.text = titles[position]
        holder.tvDescription.text = descriptions[position]
        holder.view.setOnClickListener { onListItemClick?.invoke(position) }
    }

    override fun getItemCount(): Int = titles.size

    fun updateData(titlesIn: List<String>, descriptionsIn: List<String>) {
        titles.clear()
        descriptions.clear()
        titles.addAll(titlesIn)
        descriptions.addAll(descriptionsIn)
        notifyDataSetChanged()
    }
}