package freemap.opentrail

import freemap.data.Algorithms

class AlertDisplayManager(private val distLimit: Double, private val alertDisplayCallback: (String, Int) ->Unit) {

    private var alertId = 0
    private var prevAnnotation = Feature("", Properties("", "", "0"), PointGeometry("", doubleArrayOf(0.0, 0.0)))

    fun update() {
        val features = Shared.annotations.features.clone()
        var lowestSoFar = Double.MAX_VALUE
        var lowestFeature: Feature? = null
        features.forEach {
            val dist = Algorithms.haversineDist(Shared.lon, Shared.lat, it.geometry.coordinates[0], it.geometry.coordinates[1])
            if(dist < distLimit && dist < lowestSoFar) {
                lowestFeature = it
                lowestSoFar = dist
            }
        }

        lowestFeature?.apply {
            if(this.properties.id != prevAnnotation.properties.id) {
                alertDisplayCallback(this.properties.text, ++alertId)
                prevAnnotation = this
            }
        }
    }
}