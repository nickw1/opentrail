package freemap.opentrail

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build

class AlertDisplay(private val ctx: Context) {
    private var notificationBuilder : Notification.Builder? = null

    init {
        val nMgr = ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            nMgr.createNotificationChannel(NotificationChannel("opentrail_alerts", "OpenTrail Alerts", NotificationManager.IMPORTANCE_DEFAULT))
            notificationBuilder = Notification.Builder(ctx, "opentrail_alerts")
        } else {
            notificationBuilder = Notification.Builder(ctx)
        }
    }

    fun display(msg: String, alertId: Int) {
        AlertDialog.Builder(ctx).setPositiveButton("OK", null).setMessage(msg).show()
        val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        ringtoneUri?.apply {
            val ringtone = RingtoneManager.getRingtone(ctx.applicationContext, this)
            ringtone.play()
        }

        val intent = Intent(ctx, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pendingIntent = PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = notificationBuilder?.setContentTitle("New note")?.setContentText(msg)?.setSmallIcon(R.drawable.poi)?.setContentIntent(pendingIntent)?.build()
        notification?.apply {
            val nMgr = ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nMgr.notify(alertId, this)
        }
    }
}