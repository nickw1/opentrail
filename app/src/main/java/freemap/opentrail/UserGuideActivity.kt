package freemap.opentrail

import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity


/**
 * Created by nick on 03/06/17.
 */
class UserGuideActivity : AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val wv = WebView(this)
        setContentView(wv)
        wv.loadUrl("file:///android_asset/userguide.html")
    }
}
