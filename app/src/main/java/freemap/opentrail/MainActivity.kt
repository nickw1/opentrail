package freemap.opentrail

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.preference.PreferenceManager
import com.mapzen.tangram.*
import com.mapzen.tangram.networking.DefaultHttpHandler
import com.mapzen.tangram.networking.HttpHandler
import okhttp3.*
import java.io.File
import java.util.concurrent.TimeUnit
import kotlin.math.abs


class MainActivity : AppCompatActivity(), MapView.MapReadyCallback, LocationListener {
    private var mapController: MapController? = null
    private lateinit var map1: MapView
    private var locationMarker: Marker? = null
    private var poiMarker: Marker? = null
    private val locationMarkerStyle =
        "{ style: 'points', color: 'white', size: ['24px, 33px'], order: 2000, collide: false}"
    private val poiMarkerStyle =
        "{ style: 'points', color: 'white', size: ['32px, 32px'], order: 2000, collide: false, interactive: true}"
    private val annMarkers = mutableListOf<Marker>()
    private val lastDownloadPos = LngLat(Double.MAX_VALUE, Double.MAX_VALUE)
    private val annotationDrawables = hashMapOf(
        "1" to R.drawable.caution,
        "2" to R.drawable.directions,
        "3" to R.drawable.interest
    )
    private lateinit var alertDisplay: AlertDisplay
    private lateinit var alertDisplayMgr: AlertDisplayManager
    private var freemapBaseUrl = "https://www.free-map.org.uk/fm/ws"
    private var nextAnnotationId = -1
    private lateinit var poiListResultLauncher: ActivityResultLauncher<Intent>
    private var lastLat = -91.0
    private var lastLon = -181.0
    private var sceneLoaded = false
    private lateinit var cache: Cache

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        map1 = findViewById(R.id.map1)
        map1.onCreate(savedInstanceState)

        // https://square.github.io/okhttp/caching/
        cache = Cache(File(application.cacheDir, "opentrail_cache"), 48L * 1024L * 1024L)

        map1.getMapAsync(
            this, getHttpHandler(cache)
        )

        alertDisplay = AlertDisplay(this)
        alertDisplayMgr = AlertDisplayManager(20.0, alertDisplay::display)

        poiListResultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (it.resultCode == RESULT_OK) {
                    it.data?.apply {
                        val lon = getDoubleExtra("lon", Double.MAX_VALUE)
                        val lat = getDoubleExtra("lat", Double.MAX_VALUE)
                        val name = getStringExtra("name")
                        val type = getStringExtra("type")
                        goTo(lon, lat)
                        addPoiMarker(lon, lat, name ?: "no name", type ?: "unknown")
                    }
                }
            }
    }

    override fun onMapReady(mapController: MapController?) {
        this.mapController = mapController
        this.mapController?.setSceneLoadListener { _, sceneError ->
            if (sceneError == null) {
                downloadAnnotations(true)
                sceneLoaded = true
            } else {
                AlertDialog.Builder(this).setPositiveButton("OK", null)
                    .setMessage(sceneError.error.toString()).show()
            }
        }
        showProgressBar(true)
        mapController?.loadSceneFileAsync("scene.yaml")

        mapController?.touchInput?.setTapResponder(object : TouchInput.TapResponder {

            override fun onSingleTapUp(x: Float, y: Float): Boolean {
                mapController.pickMarker(x, y)
                return true
            }

            override fun onSingleTapConfirmed(x: Float, y: Float): Boolean {
                return false
            }
        })

        mapController?.setMarkerPickListener {
            if (it?.marker?.userData != null) {
                val properties = it.marker.userData as HashMap<String, String>
                AlertDialog.Builder(this).setPositiveButton("OK", null)
                    .setMessage(properties["name"]).show()
            }
        }
        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        val cp = CameraPosition().apply {
            longitude = prefs.getFloat("lastLon", -0.72f).toDouble()
            latitude = prefs.getFloat("lastLat", 51.05f).toDouble()
            zoom = 14f
        }
        mapController?.flyToCameraPosition(cp, null)
    }

    override fun onResume() {
        super.onResume()
        map1.onResume()
        requestLocationWithPermissionCheck()
        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        prefs.getString("freemapBaseUrl", "https://www.free-map.org.uk/fm/ws")?.apply {
            if (this != freemapBaseUrl) {
                freemapBaseUrl = this
                updateScene()
            }
        }
    }

    override fun onPause() {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.removeUpdates(this)
        super.onPause()
        map1.onPause()
    }

    override fun onDestroy() {
        val editor = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit()
        editor.putFloat("lastLat", lastLat.toFloat())
        editor.putFloat("lastLon", lastLon.toFloat())
        editor.apply()

        super.onDestroy()
        map1.onDestroy()
    }

    private fun updateScene() {
        val sceneUpdates = arrayListOf(
            SceneUpdate(
                "sources.walkersmap.url",
                "$freemapBaseUrl/tsvr.php?x={x}&y={y}&z={z}&poi=place,amenity,natural,railway,power&way=highway,natural,leisure,landuse,railway,power&coastline=1&format=json&outProj=4326"
            ),
            SceneUpdate(
                "sources.terrain.url",
                "$freemapBaseUrl/tsvr.php?x={x}&y={y}&z={z}&contour=1&format=json&outProj=4326"
            )
        )
        poiMarker?.apply {
            mapController?.removeMarker(this)
        }
        poiMarker = null
        Shared.clearPois()
        clearAnnMarkers()
        mapController?.loadSceneFileAsync("scene.yaml", sceneUpdates)

    }

    private fun requestLocationWithPermissionCheck() {
        if (!requestLocation()) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                0
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 0 && permissions.isNotEmpty() && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            requestLocation()
        }
    }

    private fun requestLocation(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 2f, this)
            return true
        }
        return false
    }

    override fun onLocationChanged(loc: Location) {
        lastLon = loc.longitude
        lastLat = loc.latitude
        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        if (prefs.getBoolean("snapToGpsLocation", true)) {
            goTo(loc.longitude, loc.latitude)
        }
        setGpsLocation(loc)
        if (sceneLoaded) {
            downloadAnnotations()
        }
        alertDisplayMgr.update()
    }

    private fun setGpsLocation(loc: Location) {
        Shared.lon = loc.longitude
        Shared.lat = loc.latitude
        locationMarker?.apply {
            mapController?.removeMarker(this)
        }

        locationMarker = mapController?.addMarker()
        locationMarker?.setStylingFromString(locationMarkerStyle)
        locationMarker?.setDrawable(R.drawable.person)
        locationMarker?.setPoint(LngLat(loc.longitude, loc.latitude))
    }

    private fun gotoMyLocation() {
        if (lastLon >= -180 && lastLon <= 180 && lastLat >= -90 && lastLat <= 90) {
            goTo(lastLon, lastLat)
        } else {
            AlertDialog.Builder(this).setPositiveButton("OK", null)
                .setMessage("Location not known yet").show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val item = menu.findItem(R.id.itemSearch)
        val sv = item.actionView as SearchView

        sv.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                search(query ?: "")
                return true
            }
        })

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.itemAddAnnotation -> {
                createAddNoteDialog()
            }
            R.id.itemFindNearbyPois -> {
                findLocalPOIs()
            }
            R.id.itemSettings -> {
                Intent(this, PreferencesActivity::class.java).apply {
                    startActivity(this)
                }
            }
            R.id.itemUserGuide -> {
                Intent(this, UserGuideActivity::class.java).apply {
                    startActivity(this)
                }
            }
            R.id.itemMyLocation -> {
                gotoMyLocation()
            }
            R.id.itemAbout -> {
                AlertDialog.Builder(this).setPositiveButton("OK", null).setMessage(
                    "OpenTrail 0.6.0, using Tangram. Uses OpenStreetMap data, copyright 2004-present " +
                            "OpenStreetMap contributors, Open Database Licence. Uses " +
                            "Ordnance Survey OpenData LandForm Panorama contours, Crown Copyright. " +
                            "Person icon taken from the osmdroid project. App icon from Jason Butwell."
                ).show()
            }
            R.id.itemClearCache -> {
                cache.evictAll()
            }
        }
        return true
    }

    private fun search(q: String) {
        val url =
            "$freemapBaseUrl/search.php?q=$q&outProj=4326&poi=place,amenity,natural&format=json&remapJson=1"
        launchPoiListActivity(url, false)
    }

    private fun findLocalPOIs() {
        val bbox = BoundingBox(Shared.lon, Shared.lat, 0.1, 0.1)
        val url =
            "$freemapBaseUrl/bsvr.php?bbox=${bbox.bboxString}&outProj=4326&poi=place,amenity,natural&format=json&remapJson=1"
        launchPoiListActivity(url, true)
    }

    private fun launchPoiListActivity(url: String, storeInSharedPois: Boolean) {

        val intent = Intent(this@MainActivity, PoiSearchActivity::class.java)

        intent.putExtras(
            bundleOf(
                "url" to url,
                "storeInSharedPois" to storeInSharedPois
            )
        )
        poiListResultLauncher.launch(intent)
    }

    private fun downloadAnnotations(force: Boolean = false) {
        if (force || abs(Shared.lon - lastDownloadPos.longitude) > 0.05 || abs(Shared.lat - lastDownloadPos.latitude) > 0.05) {
            val bbox = BoundingBox(Shared.lon, Shared.lat, 0.1, 0.1)
            val url =
                "$freemapBaseUrl/bsvr.php?bbox=${bbox.bboxString}&outProj=4326&ann=1&format=json&remapJson=1"
            val downloader = GeoJsonDownloader(this, url, { showProgressBar(false) }, {
                Shared.annotations = it
                clearAnnMarkers()
                it.features.forEach { f ->
                    addAnnotation(f)
                }
            }, {
                AlertDialog.Builder(this).setPositiveButton("OK", null)
                    .setMessage("Error downloading annotations. $it").show()
            }, force)
            showProgressBar(true)
            downloader.download()

            lastDownloadPos.apply {
                longitude = Shared.lon
                latitude = Shared.lat
            }
        }
    }

    private fun addAnnotation(f: Feature) {
        val marker = mapController?.addMarker()
        marker?.apply {
            setStylingFromString(poiMarkerStyle)
            annMarkers.add(this)
            setPoint(LngLat(f.geometry.coordinates[0], f.geometry.coordinates[1]))
            userData = hashMapOf(
                "name" to f.properties.text,
                "type" to f.properties.featuretype
            )
        }
        marker?.setStylingFromString(poiMarkerStyle)
        annotationDrawables[f.properties.featuretype]?.apply {
            marker?.setDrawable(this)
        }
    }

    private fun goTo(lon: Double, lat: Double) {
        if (lon < Double.MAX_VALUE && lat < Double.MAX_VALUE) {
            mapController?.flyToCameraPosition(CameraPosition().apply {
                this.longitude = lon
                this.latitude = lat
                this.zoom = mapController?.cameraPosition?.zoom ?: 14f
            }, null)
        }
    }

    private fun addPoiMarker(lon: Double, lat: Double, name: String, type: String) {
        if (poiMarker == null) {
            poiMarker = mapController?.addMarker()
            poiMarker?.setStylingFromString(poiMarkerStyle)
            poiMarker?.setDrawable(R.drawable.poi)
        }
        poiMarker?.userData = hashMapOf("name" to name, "type" to type)
        poiMarker?.setPoint(LngLat(lon, lat))
    }

    private fun clearAnnMarkers() {
        annMarkers.forEach {
            mapController?.removeMarker(it)
        }
        annMarkers.clear()
    }

    private fun showProgressBar(show: Boolean) {
        findViewById<ProgressBar>(R.id.progressBar1).visibility =
            if (show) View.VISIBLE else View.GONE
    }


    override fun onProviderEnabled(provider: String) {
        locationMarker?.isVisible = true
    }

    override fun onProviderDisabled(provider: String) {
        locationMarker?.isVisible = false
    }

    private fun createAddNoteDialog() {
        val dialog =
            AddNoteDialogFragment(
                this,
                "$freemapBaseUrl/annotation.php",
                ::showProgressBar
            ) { lon, lat, type, text ->
                val f = Feature(
                    "Feature",
                    Properties(text, "$type", "${nextAnnotationId--}"),
                    PointGeometry("Point", doubleArrayOf(lon, lat))
                )
                addAnnotation(f)
            }
        dialog.show(supportFragmentManager, "addNote")
    }

    // Technically not needed for target API 29+, and in fact deprecated - but its absence
    // creates problems with devices running lower APIs as they expect it to be there
    // https://stackoverflow.com/questions/65700139/android-java-lang-abstractmethoderror-on-requestlocationupdates/65807111#65807111

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    // allow reloads from cache by marking cached data as stale after 30 days
    // from example
    // https://github.com/tangrams/tangram-es/blob/main/platforms/android/demo/src/main/java/com/mapzen/tangram/android/MainActivity.java#L174-L190

    private fun getHttpHandler(cache: Cache): HttpHandler {
        val builder = DefaultHttpHandler.getClientBuilder()
        builder.cache(cache)
        return object : DefaultHttpHandler(builder) {
            val cacheControl = CacheControl.Builder().maxStale(30, TimeUnit.DAYS).build()
            override fun configureRequest(url: HttpUrl, builder: Request.Builder) {
                builder.cacheControl(cacheControl)
            }
        }
    }
}