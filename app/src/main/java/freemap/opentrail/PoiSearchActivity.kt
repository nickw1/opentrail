package freemap.opentrail

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView

import freemap.data.Algorithms
import java.text.DecimalFormat


open class PoiSearchActivity : RecyclerViewActivity() {

    private var url: String? = null
    private var storeInSharedPois = false
    private lateinit var recyclerView: RecyclerView
    private var distFeatures = listOf<Pair<Feature, Double>>()
    private var downloader: GeoJsonDownloader? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        url = intent.getStringExtra("url")
        storeInSharedPois = intent.getBooleanExtra("storeInSharedPois", false)
        recyclerView = findViewById(R.id.recyclerView)
        (recyclerView.adapter as PoiAdapter).onListItemClick = {
            val returnIntent = Intent()
            returnIntent.putExtras(
                bundleOf(
                    "lat" to distFeatures[it].first.geometry.coordinates[1],
                    "lon" to distFeatures[it].first.geometry.coordinates[0],
                    "name" to distFeatures[it].first.properties.text,
                    "featuretype" to distFeatures[it].first.properties.featuretype
                )
            )
            setResult(RESULT_OK, returnIntent)
            finish()
        }

        populate()
    }

    override fun onPause() {
        downloader?.dismissDialog()
        super.onPause()
    }

    // override if we want to set the shared feature collection to the features returned
    // we do when searching for nearby POIs, but not when searching by name
    open fun populate() {
        if (this.storeInSharedPois) {
            if (Shared.featureCollection.features.isNotEmpty()) {
                doPopulate(Shared.featureCollection)
            } else {
                fetchPoiData {
                    Shared.featureCollection = it
                    doPopulate(it)
                }
            }
        } else {
            fetchPoiData(::doPopulate)
        }
    }

    private fun fetchPoiData(callback: (FeatureCollection) -> Unit) {
        showProgressBar(true)
        url?.apply {
            downloader = GeoJsonDownloader(this@PoiSearchActivity, this, {
                showProgressBar(false)
            }, callback, {
                AlertDialog.Builder(this@PoiSearchActivity).setPositiveButton("OK") { _, _ ->
                    setResult(RESULT_CANCELED)
                    finish()
                }.setMessage("Error downloading data. $it").show()
            })
            downloader?.download()
        }
    }

    private fun doPopulate(featureCollection: FeatureCollection) {
        val distFeatures =
            featureCollection.features.filter { it.properties.text != null && it.properties.text != "" && it.properties.featuretype != "unknown" }
                .map {
                    it to
                            Algorithms.haversineDist(
                                Shared.lon,
                                Shared.lat,
                                it.geometry.coordinates[0],
                                it.geometry.coordinates[1]
                            )
                }.toMutableList()

        val adapter = recyclerView.adapter as PoiAdapter

        distFeatures.sortBy { it.second }
        this.distFeatures = distFeatures
        val df = DecimalFormat("#.##")
        adapter.updateData(
            distFeatures.map {
                it.first.properties.text
            }, distFeatures.map {
                "${it.first.properties.featuretype}, distance: ${df.format(it.second * 0.001)} km"
            }
        )

    }
}