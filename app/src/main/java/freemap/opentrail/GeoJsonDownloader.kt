package freemap.opentrail

import android.app.AlertDialog
import android.content.Context
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.github.kittinunf.fuel.gson.responseObject

class GeoJsonDownloader(
    private val ctx: Context,
    private val url: String,
    private val onLoaded: () -> Unit,
    private val callback: (FeatureCollection) -> Unit,
    private val onError: (String) -> Unit,
    val showError: Boolean = true
) {

    private var dlg: AlertDialog? = null

    fun download() {
        url.httpGet().responseObject<FeatureCollection> { _, response, result ->

            onLoaded()

            when (result) {

                is Result.Failure -> {
                    if (showError) {
                        val msg =
                            "${if (response.statusCode >= 400) "HTTP code ${response.statusCode}, details ${response.responseMessage}" else "Check you have an internet connection."}"
                        onError(msg)
                    }
                }

                is Result.Success -> {
                    callback(result.get())
                }
            }
        }
    }

    fun dismissDialog() {
        dlg?.dismiss()
    }
}