package freemap.opentrail

object Shared {
    var featureCollection = FeatureCollection("FeatureCollection", arrayOf())
    var annotations = FeatureCollection("FeatureCollection", arrayOf())
    var lon = -0.72
    var lat = 51.05

    fun clearPois() {
        featureCollection = FeatureCollection("FeatureCollection", arrayOf())
    }
}