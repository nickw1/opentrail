package freemap.opentrail

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.DialogFragment
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result

class AddNoteDialogFragment(val ctx: Context, private val annotationUrl: String, private val showProgressBar: (Boolean) -> Unit, private val addAnnCallback: (Double, Double, Int, String) -> Unit) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = LayoutInflater.from(ctx)
        val view = inflater.inflate(R.layout.add_note, null)
        return AlertDialog.Builder(ctx).setView(view).setNegativeButton("Cancel", null)
            .setPositiveButton("OK") { _, _ ->
                val lon = Shared.lon
                val lat = Shared.lat
                val text = view.findViewById<EditText>(R.id.etAddNote).text.toString()
                val type = view.findViewById<Spinner>(R.id.spAddNote).selectedItemPosition + 1
                showProgressBar(true)
                annotationUrl
                    .httpPost(
                        listOf(
                            "action" to "create",
                            "text" to text,
                            "annotationType" to type,
                            "lon" to lon,
                            "lat" to lat
                        )
                    )
                    .response { _, response, result ->
                        showProgressBar(false)
                        when (result) {
                            is Result.Success -> {

                                AlertDialog.Builder(ctx).setPositiveButton("OK", null)
                                    .setMessage("Successfully added with ID ${result.get().toString(Charsets.UTF_8)}")
                                    .show()
                                addAnnCallback(lon, lat, type, text)
                            }

                            is Result.Failure -> {
                                AlertDialog.Builder(ctx).setPositiveButton("OK", null)
                                    .setMessage("ERROR HTTP code ${response.statusCode}. details ${response.responseMessage}")
                                    .show()
                            }
                        }

                    }

            }.create()
    }
}