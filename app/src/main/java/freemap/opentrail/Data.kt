package freemap.opentrail

data class Properties(val text: String, val featuretype: String, val id: String)
data class PointGeometry(val type: String, val coordinates: DoubleArray)
data class Feature (val type: String, val properties: Properties, val geometry: PointGeometry)
data class FeatureCollection(val type: String, val features:Array<Feature>)

data class BoundingBox(private val lon: Double, private val lat: Double, private val width: Double, private val height: Double) {
    val bboxString: String
        get() = "${lon-width*0.5},${lat-height*0.5},${lon+width*0.5},${lat+height*0.5}"
}
